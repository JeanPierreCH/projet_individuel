<?php session_start(); ?>

<!DOCTYPE html>
<!-- Spécifie un document HTML 5 -->

<html>
	<head>
		<!-- En-tête de la page -->

		<meta charset="utf-8" />
		
		<title>SeConnecter.php</title>
	</head>
	
	<body>
		<?php
			try {
				$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_jeanpierre", "hhva_jeanpierre", "EKolMuOFpO");
						
				$bdd->query("SET NAMES 'utf8'");
						
				$CourrielClients = $_POST['CourrielClients'];
				$MotdepasseClients = $_POST['MotdepasseClients'];
						
				$reponseDeClients = $bdd->query("SELECT * FROM utilisateur WHERE email_utilisateur = '$CourrielClients'");
				$nombreEnregistrements = $reponseDeClients->rowCount();
						
				if ($nombreEnregistrements == 0) {
					?>
						<script type="text/javascript">
							alert("Aucun utilisateur portant ce login et ce mot de passe n'est référencé. Vous devez vous enregistrer.");
							document.location.href = "SEnregistrer.html";	
						</script>
					<?php
				}
				else {
					$donneesUtilisateur = $reponseDeClients->fetch();
						
					if ($donneesUtilisateur['mot_de_passe'] == $MotdepasseClients) {
						$_SESSION['id_utilisateur'] = $donneesUtilisateur['id_utilisateur'];
								
						header("Location:Accueil.php");
					}
					else {
						?>
							<script type="text/javascript">
								alert("Votre mot de passe est erroné.");
								document.location.href = "SeConnecter.html";	
							</script>
						<?php
					}
				}
						
				$bdd = null;
			}
			catch (PDOException $e) {
				echo "Erreur !: " . $e->getMessage() . "<br />";
				die();
			}	
		?>
	</body>
</html>
