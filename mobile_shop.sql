-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 27 Août 2018 à 12:53
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `mobile_shop`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE IF NOT EXISTS `administrateur` (
  `ID_ADMINISTRATEUR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ADMIN_SUPERVISEUR` int(11) NOT NULL,
  `MDP_ADMINISTRATEUR` varchar(20) NOT NULL,
  `PSEUDO_ADMINISTRATEUR` varchar(20) NOT NULL,
  `NOM_ADMINISTRATEUR` varchar(20) NOT NULL,
  `PRENOM_ADMINISTRATEUR` varchar(20) NOT NULL,
  `DATE_NAISS_ADMINISTRATEUR` date NOT NULL,
  `SEXE_ADMINISTRATEUR` varchar(1) NOT NULL,
  `EMAIL_ADMINISTRATEUR` varchar(50) NOT NULL,
  `TELEPHONE_ADMINISTRATEUR` int(26) NOT NULL,
  PRIMARY KEY (`ID_ADMINISTRATEUR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `ID_PRODUIT` int(11) NOT NULL AUTO_INCREMENT,
  `TITRE_PRODUIT` varchar(255) NOT NULL,
  `SLUG_PRODUIT` varchar(50) NOT NULL,
  `CONTENU_PRODUIT` longtext NOT NULL,
  `DATE_CREATION_PRODUIT` datetime NOT NULL,
  `DATE_MODIFICATION_PRODUIT` timestamp NOT NULL,
  PRIMARY KEY (`ID_PRODUIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(50) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email_utilisateur` varchar(50) NOT NULL,
  `mot_de_passe` varchar(50) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `nom`, `prenom`, `email_utilisateur`, `mot_de_passe`) VALUES
(1, 'caiza Hidalgo', 'Jean', 'hidalgo@gmail.com', 'peji1207'),
(2, 'caiza', 'jean', 'caiza@gmail.com', 'peji1207'),
(3, 'caiza', 'jean', 'caiza@gmail.com', 'peji1207'),
(4, 'jean', 'caiza', 'caizah@gmail.com', 'peji1207');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
