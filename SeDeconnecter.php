<?php session_start(); ?>

<!DOCTYPE html>
<!-- Spécifie un document HTML 5 -->

<html>
	<head>
		<!-- En-tête de la page -->

		<meta charset="utf-8" />
		
		<title>SeDeconnecter.php</title>
	</head>
	
	<body>
		<?php
			unset($_SESSION['idClients']);
			
			session_destroy();
		?>
			
		<script type="text/javascript">
			alert("Nous vous remercions pour votre visite, vous êtes maintenant déconnecté.");
			document.location.href="SeConnecter.html";
		</script>
	</body>
</html>
